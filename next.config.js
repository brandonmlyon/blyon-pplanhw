/* Modules */
const withCSS = require('@zeit/next-css') /* https://nextjs.org/blog/next-7/#css-imports */
const compose = require('next-compose') /* https://github.com/JerryCauser/next-compose */
/* Configs */
const cssConfig = {
  /* https://github.com/zeit/next.js/tree/canary/examples/with-next-css */
  cssModules: true,
  webpack: function (config) {
    return config;
  }
}
/* Do it */
module.exports = compose([
  [withCSS, cssConfig],
  {
    webpack: (config) => {
      /**some special code */
      return config
    }
  }
])
