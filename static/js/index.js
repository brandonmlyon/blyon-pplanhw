
productPlanHomeworkStart = function() 
{

// POPOVERS coded from scratch

  var animationDuration = 1000;
  var currentWalkthroughStepNumber = /* read from cookie/state/db, else */ 0;
  var roadmapWrapper = document.querySelector('.roadmapWrapper');

  // I should add something to prevent non-popover clicks while the popovers are open.

  initializeWalkthrough = function() 
  {
    var currentWalkthroughStepClassName = "walkthroughStep" + currentWalkthroughStepNumber;
    var currentWalkthroughStepClass = ".walkthroughStep" + currentWalkthroughStepNumber;
    var currentWalkthroughStep = document.querySelector(currentWalkthroughStepClass);
    currentWalkthroughStep.classList.remove("hidden");
    currentWalkthroughStep.classList.add("active");
    roadmapWrapper.classList.add(currentWalkthroughStepClassName);
    document.querySelector('.roadmapLane').classList.remove("hidden"); // temporary until I implement the action which does this
  };
  initializeWalkthrough();

  walkthroughStepGotIt = function(event) 
  {
    var walkthroughWrapper = event.target.closest('.walkthroughWrapper');
    walkthroughWrapper.classList.add("hidden");
  };
  var walkthroughWrapperButtons = document.querySelectorAll('.walkthroughWrapper .button');
  for ( i=0; i < walkthroughWrapperButtons.length; i++) 
  {
    walkthroughWrapperButtons[i].addEventListener('click', walkthroughStepGotIt);
  };

  walkthroughNextStep = function() 
  {
    currentWalkthroughStepNumber ++;
    var currentWalkthroughStepClassName = "walkthroughStep" + currentWalkthroughStepNumber;
    var walkthroughWrapper = document.querySelector('.walkthroughWrapper.active');
    roadmapWrapper.className = "roadmapWrapper";
    roadmapWrapper.classList.add(currentWalkthroughStepClassName);
    if(walkthroughWrapper) 
    {
      walkthroughWrapper.classList.remove("active");
      if(walkthroughWrapper.nextSibling) 
      {
        walkthroughWrapper.nextSibling.classList.remove("hidden");
        walkthroughWrapper.nextSibling.classList.add("active");
      };
    };
    // set cookie/state/db to next step
  };

// ROADMAP inspired by http://tutorials.jenkov.com/html5/drag-and-drop.html#drag-and-drop-event-example

  // DROPABLE ITEMS
    var roadmapDroppable = document.querySelectorAll('.roadmapDroppable');
    for ( i=0; i < roadmapDroppable.length; i++) 
    {
      roadmapDroppable[i].addEventListener('dragstart', dragStart, false);
      roadmapDroppable[i].addEventListener('dragend' , dragEnd, false);
    };
    function dragStart(event) 
    {
      event.target.classList.add('dragInProgress');
      event.dataTransfer.setData('application/node type', this);
      // ^ I'm not sure if that's the correct setData.
      // There was a bug in Firefox so I copy-pasted
      // from https://stackoverflow.com/a/27789083
    }
    function dragEnd(event) 
    {
      event.target.classList.remove('dragInProgress');
    }

  // DROP TARGETS
    var laneDropTarget = document.querySelector('.laneDropTarget');
    laneDropTarget.addEventListener('dragenter', dragEnter, false);
    laneDropTarget.addEventListener('dragover' , dragOver, false);
    laneDropTarget.addEventListener('dragleave', dragLeave, false);
    laneDropTarget.addEventListener('drop', drop, false);
    var barDropTarget = document.querySelectorAll('.barDropTarget');
    for ( i=0; i < barDropTarget.length; i++) 
    {
      barDropTarget[i].addEventListener('dragenter', dragEnter, false);
      barDropTarget[i].addEventListener('dragover' , dragOver, false);
      barDropTarget[i].addEventListener('dragleave', dragLeave, false);
      barDropTarget[i].addEventListener('drop', drop, false);
    };
    function dragEnter(event) 
    {
      event.target.classList.add('dropTargetHover');
    };
    function dragOver(event) 
    {
        event.preventDefault();
        return false;
    };
    function dragLeave(event) 
    {
      event.target.classList.remove('dropTargetHover');
    };
    function drop(event) 
    {
        event.target.classList.remove('dropTargetHover');
        if(event.target.classList.contains('laneDropTarget'))
        {
          event.target.classList.add('hidden');
          document.querySelector('.roadmapDroppableLane').setAttribute('draggable', false);
          document.querySelector('.roadmapDroppableLane').setAttribute('disabled', true);
        }
        event.target.classList.add('done');
        // note there's significantly more to do here but since this a shot demo let's just advance the steps...
        walkthroughNextStep();
        var data = event.dataTransfer.getData('text/html');
        event.preventDefault();
        return false;
    };

}; // end productPlanHomeworkStart

productPlanHomeworkStart();
