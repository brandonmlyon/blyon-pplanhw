import '../static/css/normalize.css'
import '../static/css/fontStack.css'
import '../static/css/variables.css'
import '../static/css/buttons.css'
import '../static/css/components/LayoutMain.css'
import HeadConfig from './HeadConfig'
import Header from './Header'

const Layout = (props) => (
  <div>
    <HeadConfig />
    <Header />
    {props.children}
  </div>
)

export default Layout
