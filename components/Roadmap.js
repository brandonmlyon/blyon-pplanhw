import '../static/css/components/Roadmap.css'

const Layout = (props) => (
   <div class="roadmapWrapper">
      <div class="roadmapLanes">
         <div class="roadmapLaneLabels">
            <span>Q1 2019</span>
            <span></span>
            <span></span>
            <span>Q2</span>
            <span></span>
            <span></span>
            <span>Q3</span>
            <span></span>
            <span></span>
            <span>Q4</span>
            <span></span>
            <span></span>
            <span>Q1 2020</span>
            <span></span>
            <span></span>
            <span>Q2</span>
            <span></span>
            <span></span>
            <span>Q3</span>
            <span></span>
            <span></span>
            <span>Q4</span>
            <span></span>
            <span></span>
         </div>
         <div class="laneDropTarget"></div>
         <div class="roadmapLane hidden">
            <div class="roadmapLaneTitle"><span class="roadmapLaneTitleIcon"></span><span class="roadmapLaneTitleValue">Lane</span></div>
            <div class="roadmapLaneInner">
               <div class="barDropTarget barTarget0"></div>
               <div class="barDropTarget barTarget2"></div>
               <div class="barDropTarget barTarget1"></div>
            </div>
         </div>
      </div>
      <div class="roadmapGarage">
         <div class="roadmapDroppable roadmapDroppableLane" draggable="true"></div>
         <div class="roadmapDroppable roadmapDroppableBar" draggable="true"></div>
      </div>
   </div>
)

export default Layout
