describe('EnsurePageHasTitle', function() {
   it('Ensures the page has a title' , function() {
      cy.visit('http://localhost:8080')

      cy.title()
         .should('match', /[a-zA-Z]/)

   })
})
