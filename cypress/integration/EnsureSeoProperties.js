describe('EnsureSeoProperties', function() {
   it('Ensures the page has SEO properties set' , function() {
      cy.visit('http://localhost:8080')
      cy.log('NOTE! SEARCHING FOR INDEX OR FOLLOW')
      cy.log('WILL RETURN TRUE IF NOINDEX OR NOFOLLOW')

      cy.get('meta[name="robots"]')
         .should('have.attr', 'content')
         .and('include', 'index')
         .and('include', 'follow')
   })
})
